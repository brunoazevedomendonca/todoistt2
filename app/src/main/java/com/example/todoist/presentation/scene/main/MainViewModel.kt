package com.example.todoist.presentation.scene.main

import androidx.lifecycle.ViewModel
import com.example.todoist.data.repository.TodoistRepository
import com.example.todoist.presentation.common.Screens
import com.github.terrakok.cicerone.Router
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val router: Router
) : ViewModel() {
    init {
        router.replaceScreen(Screens.projectsScreen())
    }

}